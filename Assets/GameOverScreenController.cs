﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreenController : MonoBehaviour
{

    [SerializeField]
    private Text text;
    [SerializeField]
    private GameObject gameOverScreen;

    // Use this for initialization
    public void Init(int points)
    {
        StartCoroutine(Restart());
        gameOverScreen.SetActive(true);
        text.text = "Your Score: " + points;
    }

    private IEnumerator Restart()
    {
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Creepy_v0.1");
    }
}
