﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCreep : MonoBehaviour {

    [SerializeField]
    private GoToExit goToExit;

    [SerializeField]
    private AudioSource source;

	// Use this for initialization
	void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Creep")
        {
            source.Play();
            ++goToExit.points;
            Destroy(col.gameObject);
        }
    }
}
