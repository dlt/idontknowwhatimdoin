﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayMovie : MonoBehaviour
{
    public MovieTexture movTexture;
    public AudioClip clip;
    public AudioSource source;
    public Image image;

    void Start () {
        image.material.mainTexture = movTexture;
        movTexture.Play();
        source.Play();

        StartCoroutine(OpenMenu());
    }

    private IEnumerator OpenMenu()
    {
        yield return new WaitForSeconds(10.0f);
        SceneManager.LoadScene("menu2");
    }
	
}
