﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoToExit : MonoBehaviour {

    [SerializeField]
    private DestroyCreep target;
    [SerializeField]
    private GameOverScreenController gameOverScreen;

    private float timer = 30.0f;

    public int points = 0;

    private void Update()
    {
        if (timer <= 0)
            gameOverScreen.Init(points);
        timer -= Time.deltaTime;
    }

	void OnTriggerStay(Collider col)
    {
        if (col.tag == "Creep")
        {
            col.GetComponent<NavMeshAgent>().SetDestination(target.transform.position);
        }
    }
}
