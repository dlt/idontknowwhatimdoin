﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    [SerializeField]
    private GameObject CreepPrefab;

	// Use this for initialization
	void Start ()
    {
      
        var number = Random.Range(30, 40);
        //Debug.Log(number);
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomDirection = Random.insideUnitSphere * 30;
            //Debug.Log(randomDirection);
            randomDirection += transform.position;
            UnityEngine.AI.NavMeshHit hit;
            UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, 30, 1);
            GameObject creep = (GameObject)Instantiate(CreepPrefab);
            creep.transform.position = hit.position;
        }
   
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
