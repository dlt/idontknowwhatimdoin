﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{

    [SerializeField]
    private GameObject viewPoint;
    [SerializeField]
    private float range;
    [SerializeField]
    private float maxCandyAmount;
    [SerializeField]
    private float speed;
    [SerializeField]
    private GameObject throwPoint;
    [SerializeField]
    private List<GameObject> thrownObjects;
    [SerializeField]
    private AudioSource throwSource;

    private Renderer dispenserRenderer = null;
    //private Color lastColor;
    private float candyAmount=0;
    private bool firstFrame;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        RaycastHit hit;
        bool raycastResult = Physics.Raycast(viewPoint.transform.position, viewPoint.transform.forward, out hit, range);

        if (raycastResult)
        {
            if (!dispenserRenderer && hit.collider.tag == "Dispenser")
            {
                dispenserRenderer = hit.collider.GetComponent<Renderer>();
                //lastColor = dispenserRenderer.material.color;
                dispenserRenderer.material.SetFloat("_Highlight", 1.0f);

                //dispenserRenderer.material.color = Color.blue;
            }
        }
        else if (dispenserRenderer)
        {
            dispenserRenderer.material.SetFloat("_Highlight", 0.0f);
            dispenserRenderer = null;
        }

        if (Input.GetKeyDown("mouse 0"))
        {
            Debug.Log(candyAmount);
            if (raycastResult && hit.collider.tag == "Dispenser")
            {
                candyAmount = maxCandyAmount;
            }
            else if (candyAmount > 0)
            {
                throwSource.Play();
                candyAmount--;
                var obj = Instantiate(thrownObjects[Random.Range(0, thrownObjects.Count)], viewPoint.transform.position + viewPoint.transform.forward, viewPoint.transform.rotation);
                obj.GetComponent<Rigidbody>().AddForce(viewPoint.transform.forward * speed);
            }
        }

    }
}
