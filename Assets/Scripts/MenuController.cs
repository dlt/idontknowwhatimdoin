﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Texture GameLogo;

    public void Exit()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Creepy_v0.1");
    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(200, 0, 320, 180), GameLogo);
    }

}
