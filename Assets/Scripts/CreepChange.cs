﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        gameObject.transform.localPosition = new Vector3(0f, 0.15f + 0.15f * Mathf.Sin(Time.time * 8), 0f) / 30.0f;

        var xz = 0.8f - 0.2f * Mathf.Sin(Time.time * 8);
        gameObject.transform.localScale = new Vector3(xz, 0.8f + 0.2f * Mathf.Sin(Time.time * 8), xz);

    }
}
