﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menu : MonoBehaviour
{

    public Texture GameLogo;
    public float buttonWidth = 300;
    public float buttonHeight = 60;

    private float buttonMargin = 20;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, 800, 300), GameLogo);
        if (GUI.Button(new Rect(200, 300, buttonWidth, buttonHeight), "New Game"))
        {
            Application.LoadLevel("Main");
        }
        if (GUI.Button(new Rect(300, 300 + buttonHeight + buttonMargin, buttonWidth, buttonHeight), "Options"))
        {

        }
        if (GUI.Button(new Rect(400, 300 + (buttonHeight + buttonMargin) * 2, buttonWidth, buttonHeight), "Exit"))
        {
            Application.Quit();
        }
    }
}