﻿using UnityEngine;
using System.Collections;

public class eat : MonoBehaviour
{
    [SerializeField]
    private float life;
    [SerializeField]
    private float eatingSpeed;
    Rigidbody rigidb;

    void Start() {
        rigidb = GetComponent<Rigidbody>();
    }

	void Update(){

        if (life <= 0.0f)
            Destroy(gameObject);   
	}

    void OnTriggerStay(Collider other){

        if (other.tag == "Creep") {
            life -= eatingSpeed;
        }
        if (other.tag == "Floor") {
            gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            rigidb.velocity = new Vector3(0,0,0);
        }
    }
}
