﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepsMove : MonoBehaviour {

    [SerializeField]
    private UnityEngine.AI.NavMeshAgent agent;
    [SerializeField]
    private AudioSource source;

    // Use this for initialization
    void Start () {

        //agent.SetDestination(agent.transform.position + new Vector3(5f, 0f, 8f));
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Candy")
            source.Play();
    }

    void OnTriggerStay(Collider col)
    {
        if (!agent.isActiveAndEnabled) return;
        //Debug.Log(col);
        if (col.tag == "Candy")
        {
            //Debug.Log("done");
            agent.SetDestination(col.gameObject.transform.position);
        }

        if (col.tag == "Exit")
        {
            agent.SetDestination(new Vector3(-26.44f, -1f, -6.56f));
        }

        //else Debug.Log("nope");
    }

	// Update is called once per frame
	void Update () {
        if (!agent.hasPath || agent.pathStatus != UnityEngine.AI.NavMeshPathStatus.PathComplete)
        {
            agent.SetDestination(transform.position + new Vector3(Random.Range(-5.0f, 5.0f), 0, Random.Range(-5.0f, 5.0f)));   
        }
    }
}
